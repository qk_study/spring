package com.wangqk;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.wangqk.bean.Persion;
import com.wangqk.config.MainConfig;

public class MainTest {
	public static void main(String[] args) {
		//Old();
		New();
	}

	private static void New() {
		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(MainConfig.class);
		Persion bean = annotationConfigApplicationContext.getBean(Persion.class);
		System.out.println(bean);
		System.out.println("------------");
		String[] forTypeName = annotationConfigApplicationContext.getBeanNamesForType(Persion.class);
		for( String name:forTypeName) {
			System.out.println(name);
		}
		System.out.println("------------");
	}

	private static void Old() {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("beans.xml");

		Persion bean = (Persion) applicationContext.getBean("person");
		System.out.println(bean);
	}

}
